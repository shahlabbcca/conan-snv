/* The MIT License

   Copyright (c) 2009, by Sohrab Shah <sshah@bccrc.ca> and Rodrigo Goya <rgoya@bcgsc.ca>

   Permission is hereby granted, free of charge, to any person obtaining
   a copy of this software and associated documentation files (the
   "Software"), to deal in the Software without restriction, including
   without limitation the rights to use, copy, modify, merge, publish,
   distribute, sublicense, and/or sell copies of the Software, and to
   permit persons to whom the Software is furnished to do so, subject to
   the following conditions:

   The above copyright notice and this permission notice shall be
   included in all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
   EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
   BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
   ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
   SOFTWARE.
*/

/*
C Implementation of SNVMix2, includes CoNAn and SNVMix1 models
*/

#define VERSION "0.11.9-r1"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "SNVMixsuite.h"

#define START_QNUM 1000

int main(int argc, char **argv) {
	
	param_struct params;// = {NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0};

	initSNVMix(argc, argv, &params);

	if(params.classify || params.filter) {
		if(params.input_type == Q_PILEUP)
			snvmixClassify_qualities(&params);
		else if(params.input_type == M_PILEUP || params.input_type == S_PILEUP)
			snvmixClassify_pileup(&params);
	} else if(params.train) {
		if(params.input_type == M_PILEUP || params.input_type == S_PILEUP)
			snvmixTrain_pileup(&params);
		else if(params.input_type == Q_PILEUP) {
			fprintf(stderr,"Sorry, Training with quality-type input is not yet implemented\n");
			exit(1);
		}
	}

}

/*
	void snvmixClassify_qualities
	Arguments:
		*params : pointer to model parameters and command line options
	Function:
		Reads a pilep-style file that has been preprocessed to include the quality of
		calls being the reference allele and the maping quality both in decimal values.

		For each line it evaluates de model according to the parameters in *params and
		outputs the corresponding SNV prediction values.

		This function may come in handy when filtering of calls is done by a
		different program or the base-calling and maping quality information is not
		in a pileup/phred format.
	
		The file read is a tab-separated file where the columns are:
			-  target sequence (e.g. chromosome)
			-  sequence position
			-  reference base
			-  non-reference base
			-  comma separated probabilities of the call being the reference
			-  comma separated mapping qualities, one for each base call

*/
void snvmixClassify_qualities(param_struct *params) {
	FILE *fptrIN, *fptrOUT;

	char *line = NULL;
	size_t line_len = 0, prev_len = 0;
	int read_len = 0;

	int qual_num = 0, col_num = 0;

	char *col, *qual;
	char *col_sep = "\t", *col_str, *col_ptr;
	char *qual_sep = ",", *qual_str, *qual_ptr;
	char *chr, *pos, *ref, *nref;

	long double *bQ, *mQ, *tmpQ;
	int pos_int, depth = 0, maxp;
	long int depth_allocated = 0;

	long double bsum = 0, msum = 0, **b, **mu, **notmu, **pi, **log_pi, z = 0;
	int i, k, genotypes, cn_states, cn;

	cnv_segment_t cnv_segment;
	char *cnv_segment_chr = NULL;

	// Initialize local values of parameters
	mu = params->mu;
	pi = params->pi;
	cn_states = params->max_cn + 1;

	if( ( b = malloc(cn_states * sizeof(long double *)) ) == NULL) { perror("[snvmixClassify_qualities] Could not allocate b\n"); exit(1); }
	if( ( notmu = malloc(cn_states * sizeof(long double *)) ) == NULL) { perror("[snvmixClassify_qualities] Could not allocate notmu\n"); exit(1); }
	if( ( log_pi = malloc(cn_states * sizeof(long double *)) ) == NULL) { perror("[snvmixClassify_qualities] Could not allocate log_pi\n"); exit(1); }

	for(cn = params->min_cn; cn <= params->max_cn; cn++) {
		if( ( b[cn] = malloc((cn+1) * sizeof(long double)) ) == NULL) { perror("[snvmixClassify_qualities] Could not allocate b[cn]\n"); exit(1); }
		if( ( notmu[cn] = malloc((cn+1) * sizeof(long double)) ) == NULL) { perror("[snvmixClassify_qualities] Could not allocate notmu[cn]\n"); exit(1); }
		if( ( log_pi[cn] = malloc((cn+1) * sizeof(long double)) ) == NULL) { perror("[snvmixClassify_qualities] Could not allocate log_pi[cn]\n"); exit(1); }
		for(k = 0; k <= cn; k++) {
			notmu[cn][k] = 1 - mu[cn][k];
			log_pi[cn][k] = logl(pi[cn][k]);
		}
	}
	fptrIN = params->input;
	fptrOUT = params->output;


	if( (bQ = malloc(START_QNUM * sizeof (long double))) == NULL) {
		perror("ERROR allocating initial space for bQ"); exit(1);
	}
	if( (mQ = malloc(START_QNUM * sizeof (long double))) == NULL) {
		perror("ERROR allocating initial space for mQ"); exit(1);
	}
	depth_allocated = START_QNUM;
#if defined __linux__ || defined _GNU_SOURCE
	while( (read_len = getline(&line,&line_len,fptrIN)) != -1 )
#endif
#ifdef __APPLE__
	while( (line = fgetln(fptrIN,&line_len)) != NULL )
#endif
		{
		line[line_len-1] = '\0';
		depth = 0;
		col_ptr = NULL;
		for(col_num = 0, col_str = line; ; col_num++, col_str = NULL) {
			col = strtok_r(col_str, "\t", &col_ptr);
			if(col == NULL) {
				break;
			}
			if(col_num == 0) {
				chr = col;
			} else if(col_num == 1) {
				pos = col;
				pos_int = strtol(pos, NULL, 10);
			} else if(col_num == 2) {
				ref = col;
			} else if(col_num == 3) {
				nref = col;
			} else if(col_num == 4) {
				for(qual_num = 0, qual_str = col; ; qual_num++, qual_str = NULL) {
					qual = strtok_r(qual_str, ",", &qual_ptr);
					if(qual == NULL) {
						break;
					}
					if(qual_num >= depth_allocated) {
						if( (bQ = realloc(bQ, sizeof(long double) * (depth_allocated + START_QNUM))) == NULL) {
							perror("ERROR allocating bQ"); exit(1);
						}
						if( (mQ = realloc(mQ, sizeof(long double) * (depth_allocated + START_QNUM))) == NULL) {
							perror("ERROR allocating mQ"); exit(1);
						}
						depth_allocated = depth_allocated + START_QNUM;
					}
					bQ[qual_num] = atof(qual);
				}
				depth = qual_num;
			} else if(col_num == 5) {
				for(qual_num = 0, qual_str = col; ; qual_num++, qual_str = NULL) {
					qual = strtok_r(qual_str, ",", &qual_ptr);
					if(qual == NULL) {
						break;
					}
					if(qual_num >= depth_allocated) {
						fprintf(stderr, "FATAL ERROR: should not have more mapping qualities than base qualities\n");
						exit(1);
					}
					mQ[qual_num] = atof(qual);
				}
				if(depth != qual_num) {
					fprintf(stderr, "FATAL ERROR: should not have more base qualities than mapping qualities\n");
					exit(1);
				}
			}
		}

		// Get CNV at this position
		if(params->cnv) {
			if(pos_int < cnv_segment.l || pos_int > cnv_segment.u || (cnv_segment_chr == NULL || strcmp(chr,cnv_segment_chr))) {
				get_cn_state(params, chr, pos_int, &cnv_segment);
				cn = cnv_segment.cn;
				free(cnv_segment_chr); cnv_segment_chr = strdup(chr);
			}
		} else { cn = 2; }

		for(k = 0; k <= cn; k++)
			b[cn][k] = 0;
		
    		//b = sum(log(notm*0.5 + m.*(yr.*mur+notyr.*notmur)),2);
		for(qual_num = 0; qual_num < depth; qual_num++)
			for(k = 0; k <= cn; k++)
				b[cn][k] = b[cn][k] + logl((1- mQ[qual_num])*0.5 + mQ[qual_num] * (bQ[qual_num]*mu[cn][k]+(1-bQ[qual_num])*notmu[cn][k]));

		z = 0;
		for(k = 0; k <= cn; k++) {
			b[cn][k] = expl(b[cn][k] + log_pi[cn][k]);
			z += b[cn][k];
		}
		if(!z) { z = 1; }
		for(k = 0; k <= cn; k++)
			b[cn][k] = b[cn][k] / z;
		maxp = 1;
		z = b[cn][0];
		for(k = 1; k <= cn; k++)
			if( b[cn][k] > z) {
				maxp = k+1;
				z = b[cn][k];
			}

		fprintf(fptrOUT,"%s\t%s\t%s\t%s",chr, pos, ref, nref);
		for(k = 0; k <= cn; k++)
			fprintf(fptrOUT,"%0.10Lf,",b[cn][k]);
		fprintf(fptrOUT,"%d\n",maxp);
	}
	fclose(fptrIN);
	fclose(fptrOUT);
	free(line);
	for(cn = params->min_cn; cn <= params->max_cn; cn++) {
		free(b[cn]); free(notmu[cn]); free(log_pi[cn]);
	}
	free(b); free(notmu); free(log_pi);
}


/*
	void snvmixClassify_pileup
	Arguments:
		*params : pointer to model parameters and command line options
	Function:
		Reads a MAQ or Samtools pileup format. For required formatting we refer the user
		to the corresponding websites:
			http://maq.sourceforge.net/
			http://samtools.sourceforge.net/

		In general the format of both files can be described as a tab separated file
		where columns are:
			-  target sequence (e.g. chromosome)
			-  sequence position
			-  reference base
			-  depth
			-  stream of base calls
			-  stream of base call PHRED-scaled qualities
			-  stream of mapping PHRED-scaled qualities

		Note: when handling pileup files, care should be taken when doing copy-paste
		operations not to transform 'tabs' into streams of spaces.

		For each line it evaluates de model according to the parameters in *params and
		outputs the corresponding SNV prediction values.

		Predictions can be output either only for positions where when non-reference values
		are observed or, when run with '-f' flag, for all positions. The -f flag is useful
		when the resulting calls are being compared or joined accros different datasets
		and all predictions need to be present.
*/
void snvmixClassify_pileup(param_struct *params) {
//MAQ:
//	1       554484  C       1752    @,,.,.,, @xxx @xxx xxxxx
//SAM:
//	1       554484  C       1752    ,,.,.,,	xxx xxxx
	FILE *fptrIN, *fptrOUT;

	char *line = NULL;
	size_t line_len = 0, prev_len = 0;
	int read_len = 0;
	int col_num = 0;
	long int line_num = 0;

	char *col, *qual;
	char *col_sep = "\t", *col_str, *col_ptr;
	char *qual_sep = ",", *qual_str, *qual_ptr;
	char *chr, *pos, *ref, nref, call, nref_skip = 'N';

	char *bQ, *mQ;
	int *calls, *tmpQ;
	int pos_int, depth = 0, qual_num,  maxp;
	int depth_allocated = 0;

	long double bsum = 0, msum = 0, **b, **mu, **notmu, **pi, **log_pi, z = 0;
	int nref_count = 0, ref_count = 0;
	long double Y, not_Y;
	long double phred[PHRED_MAX + 1];
	int i, call_i, k, cn_states, cn;

	cnv_segment_t cnv_segment;
	char *cnv_segment_chr = NULL;

	//initPhred(phred, PHRED_MAX+1);
	initPhred(phred, PHRED_MAX);


	// Initialize local values of parameters
	mu = params->mu;
	pi = params->pi;
	cn_states = params->max_cn + 1;

	if( ( b = malloc(cn_states * sizeof(long double *)) ) == NULL) { perror("[snvmixClassify_pileup] Could not allocate b\n"); exit(1); }
	if( ( notmu = malloc(cn_states * sizeof(long double *)) ) == NULL) { perror("[snvmixClassify_pileup] Could not allocate notmu\n"); exit(1); }
	if( ( log_pi = malloc(cn_states * sizeof(long double *)) ) == NULL) { perror("[snvmixClassify_pileup] Could not allocate log_pi\n"); exit(1); }

	for(cn = params->min_cn; cn <= params->max_cn; cn++) {
		if( ( b[cn] = malloc((cn+1) * sizeof(long double)) ) == NULL) { perror("[snvmixClassify_pileup] Could not allocate b[cn]\n"); exit(1); }
		if( ( notmu[cn] = malloc((cn+1) * sizeof(long double)) ) == NULL) { perror("[snvmixClassify_pileup] Could not allocate notmu[cn]\n"); exit(1); }
		if( ( log_pi[cn] = malloc((cn+1) * sizeof(long double)) ) == NULL) { perror("[snvmixClassify_pileup] Could not allocate log_pi[cn]\n"); exit(1); }
		for(k = 0; k <= cn; k++) {
			notmu[cn][k] = 1 - mu[cn][k];
			log_pi[cn][k] = logl(pi[cn][k]);
		}
	}
	fptrIN = params->input;
	fptrOUT = params->output;
	if(params->full)
		nref_skip = -2;


	if( (calls = malloc(START_QNUM * sizeof (int))) == NULL) {
		perror("ERROR allocating initial space for calls"); exit(1);
	}
	depth_allocated = START_QNUM;
#if defined __linux__ || defined _GNU_SOURCE
	while( (read_len = getline(&line,&line_len,fptrIN)) != -1 )
#endif
#ifdef __APPLE__
	while( (line = fgetln(fptrIN,&line_len)) != NULL )
#endif
	{
		line[line_len-1] = '\0';
		line_num++;
		depth = 0;
		nref = 0;
		col_ptr = NULL;
		for(col_num = 0, col_str = line; nref != nref_skip ; col_num++, col_str = NULL) {
			col = strtok_r(col_str, "\t", &col_ptr);
			if(col == NULL) {
				break;
			}
			if(col_num == 0) {
				chr = col;
			} else if(col_num == 1) {
				pos = col;
				pos_int = strtol(pos, NULL, 10);
			} else if(col_num == 2) {
				ref = col;
			} else if(col_num == 3) {
				depth = atoi(col);
				if(depth > depth_allocated) {
					if( (calls = realloc(calls, sizeof (int) * (depth + START_QNUM))) == NULL) {
						perror("ERROR allocating space for calls"); exit(1);
					}
					depth_allocated = depth + START_QNUM;
				}
			} else if(col_num == 4) {
				if(params->input_type == M_PILEUP)
					col = col+sizeof(char);
					snvmixGetCallString(col, calls, depth, &nref);
			} else if(col_num == 5) {
				bQ = col;
				// If it's a MAQ pileup, we need to skip the @ sign
				if(params->input_type == M_PILEUP)
					bQ = bQ + sizeof(char);
				for(call_i = 0; call_i < depth; call_i++)
					bQ[call_i] = bQ[call_i]-33;
			} else if(col_num == 6) {
				mQ = col;
				// If it's a MAQ pileup, we need to skip the @ sign
				if(params->input_type == M_PILEUP)
					mQ = mQ + sizeof(char);
				for(call_i = 0; call_i < depth; call_i++)
					mQ[call_i] = mQ[call_i]-33;
			}
		}
		// If we quit the for because no nref was found, skip this too, next line
	nref = snvmixFilterCalls(calls,depth,bQ,mQ,params);
	nref_count = 0;
	ref_count = 0;
	for(qual_num = 0; qual_num < depth; qual_num++) {
		//if( snvmixSkipCall(calls,qual_num,params,bQ,mQ) )
		if(calls[qual_num] == -1)
			continue;
		if(calls[qual_num] == 1)
			ref_count++;
		if(calls[qual_num] == nref)
			nref_count++;
	}
if(params->filter) {
	fprintf(fptrOUT,"%s:%s\t%s:%d\t%c:%d\t", chr, pos, ref, ref_count, nref, nref_count);
	for(qual_num = 0; qual_num < ref_count; qual_num++)
		fprintf(fptrOUT,",");
	for(qual_num = 0; qual_num < nref_count; qual_num++)
		fprintf(fptrOUT,"%c",nref);
	fprintf(fptrOUT,"\n");
} else {
		if(nref == nref_skip)
			continue;
		//nref = snvmixFilterCalls(calls,depth,bQ,mQ,params);
		//if(nref == nref_skip)
		//	continue;
		// Get CNV at this position
		if(params->cnv) {
			if(pos_int < cnv_segment.l || pos_int > cnv_segment.u  || (cnv_segment_chr == NULL || strcmp(chr,cnv_segment_chr))) {
				get_cn_state(params, chr, pos_int, &cnv_segment);
				cn = cnv_segment.cn;
				free(cnv_segment_chr); cnv_segment_chr = strdup(chr);
			}
		} else { cn = 2; }

		for(k = 0; k <= cn; k++)
			b[cn][k] = 0;
		nref_count = 0;
		for(qual_num = 0; qual_num < depth; qual_num++) {
			//if( snvmixSkipCall(calls,qual_num,params,bQ,mQ) )
			if(calls[qual_num] == -1)
				continue;
		// from matlab:
		// b = sum(log(notm*0.5 + m.*(yr.*mur+notyr.*notmur)),2);
			if(calls[qual_num] == 1) {
				// If REF then
				Y = phred[(unsigned char) bQ[qual_num]];
				not_Y = 1-phred[(unsigned char) bQ[qual_num]];
			} else {
				nref_count++;
				// If NREF then
				Y = (1-phred[(unsigned char) bQ[qual_num]])/3;
				not_Y = phred[(unsigned char) bQ[qual_num]] + 2*(1-phred[(unsigned char)bQ[qual_num]])/3;
			}
			for(k = 0; k <= cn; k++)
				b[cn][k] = b[cn][k] + logl((1-phred[(unsigned char) mQ[qual_num]])*0.5 + phred[(unsigned char) mQ[qual_num]] * (Y * mu[cn][k] + not_Y * notmu[cn][k]));
		}
		// Check if any non-references actually passed the filter
		//if(!nref_count)
		//	continue;
		z = 0;
		for(k = 0; k <= cn; k++) {
			b[cn][k] = expl(b[cn][k] + log_pi[cn][k]);
			z += b[cn][k];
		}
		if(!z) z = 1;
		for(k = 0; k <= cn; k++)
			b[cn][k] = b[cn][k] / z;
		maxp = 1;
		z = b[cn][0];
		for(k = 1; k <= cn; k++)
			if( b[cn][k] > z) {
				maxp = k+1;
				z = b[cn][k];
			}


		fprintf(fptrOUT,"%s:%s\t%s\t%c\t%s:%d,%c:%d,",chr,pos, ref, nref, ref, ref_count,  nref, nref_count);
		for(k = 0; k <= cn; k++)
			fprintf(fptrOUT,"%0.10Lf,",b[cn][k]);
		fprintf(fptrOUT,"%d\n",maxp);
}
	}
	fclose(fptrIN);
	fclose(fptrOUT);
	free(line);
	for(cn = params->min_cn; cn <= params->max_cn; cn++) {
		free(b[cn]); free(notmu[cn]); free(log_pi[cn]);
	}
	free(b); free(notmu); free(log_pi);
}

/*
	void snvmixGetCallString
	Arguments:
		*col : pointer to the current file-line in memory
		*calls : pointer to array where we will store the final base-calls
		depth : number of base reads for this position
		*nref : the observed NREF value will be placed here (-1 if none was found)

	Function:
		This will parse column 5 of the pileup file, which contains the
		base calls and will fill the array "calls" with:
			1 : when a reference call was made
		       -1 : when an invalid value is seen ('N', 'n', '*') 
		   [ACTG] : when a non-reference call was made

		

*/
void snvmixGetCallString(char *col, int *calls, int depth, char *nref) {
	int i;
	int call_i = 0, call_skip_num = 0;
	char call_skip_char[10];
	for(i = 0 ; call_i < depth; i++) {
		switch(col[i]){
		case ',':
		case '.':
			calls[call_i] = 1;
			call_i++;
			break;
		case 'a': case 'A':
		case 't': case 'T':
		case 'g': case 'G':
		case 'c': case 'C':
			//calls[call_i] = 0;
			calls[call_i] = toupper(col[i]);
			call_i++;
			//if(*nref == 0)
				//*nref = toupper(*(col+sizeof(char)*i));
			if(*nref == 0)
				*nref = toupper(col[i]);
				break;
		case 'N':
		case 'n':
		case '*':
			// Not comparable values, we ignore them, but need to
			// keep count to compare against the number of mapping qualities
			calls[call_i] = -1;
			call_i++;
			break;
		case '$':
			// End of a read, just ignore it
			break;
		case '^':
			// Start of read, ignore it and skip the next value (not base-related)
			i++;
			break;
		case '+':
		case '-':
			// Eliminate:
			//		+2AA
			//		-3AAA
			// Start skiping the sign
			i++;
			for(call_skip_num = 0; col[i] <= '9' && col[i] >= '0'; call_skip_num++, i++) {
				//call_skip_char[call_skip_num] = call;
				call_skip_char[call_skip_num] = col[i];
				//i++;
			}
			// we have the number string in call_skip_char, lets terminate it
			call_skip_char[call_skip_num] = '\0';
			// i has been updated to first letter, just need to jump the rest of them
			i = i+atoi(call_skip_char)-1;
			break;
		default:
			fprintf(stderr,"ERROR: problem reading pileup file calls (%c)\n",col[i]);
			exit(1);
			break;
		}
	}
	// If no nref was found, don't bother parsing the other columns, make the for fail with -1
	if(!*nref)
		*nref = -1;
}

/*
	int snvmixFilterCalls
	Arguments:
		*calls : array built by snvmixGetCallString containing
			1 : when a reference call was made
		       -1 : when an invalid value is seen ('N', 'n', '*') 
		   [ACTG] : when a non-reference call was made
		depth : number of calls for the current position
		*bQ : base qualities observed for each call
		*mQ : map quality for each call
		*params : parameter structure, get thresholding data
	Function:
		For each valid call read in the position this function will apply
		thresholding according to the type selected (-t flag) and the bQ (-q)
		and mQ (-Q) thresholds provided.

		Any base-call that does not pass thresholds will be switched from it's
		current value in *calls to -1;

		The most prevalent NREF after filtering will be determined and returned.
	
*/
int snvmixFilterCalls(int *calls, int depth, char *bQ, char *mQ, param_struct *params) {
	int qual_num, nref_counts[5];
	nref_counts[0] = 0;
	nref_counts[1] = 0;
	nref_counts[2] = 0;
	nref_counts[3] = 0;
	nref_counts[4] = 0;
	int max_nref = N;

	char nref = 0;

	for(qual_num = 0; qual_num < depth; qual_num++) {
		if( snvmixSkipCall(calls,qual_num,params,bQ,mQ) ) {
			calls[qual_num] = -1;
		} else {
			nref_counts[0]++;
			switch(calls[qual_num]) {
				case 'A':
					nref_counts[A]++;
					break;
				case 'C':
					nref_counts[C]++;
					break;
				case 'G':
					nref_counts[G]++;
					break;
				case 'T':
					nref_counts[T]++;
					break;
				case 1:
				case -1:
					nref_counts[0]--;
					break;
				default:
					fprintf(stderr,"ERROR: unknown call base\n");
					exit(1);
			}
		}
	}
	if(nref_counts[0]) {
		max_nref = A;
		if(nref_counts[max_nref] < nref_counts[C])
			max_nref = C;
		if(nref_counts[max_nref] < nref_counts[G])
			max_nref = G;
		if(nref_counts[max_nref] < nref_counts[T])
			max_nref = T;
	} else {
		//return -1;
	}
	for(qual_num = 0; qual_num < depth; qual_num++) {
		if(calls[qual_num] == 1)
			continue;
		if(calls[qual_num] != base_code[max_nref])
			calls[qual_num] = -1;
	}
	return base_code[max_nref];
}

/*
	int snvmixSkipCall
	Arguments:
		*calls : array built by snvmixGetCallString containing
			1 : when a reference call was made
		       -1 : when an invalid value is seen ('N', 'n', '*') 
		   [ACTG] : when a non-reference call was made
		qual_num : call number that is being evaluated
		*params : parameter structure, get thresholding data
		*bQ : base qualities observed for each call
		*mQ : map quality for each call
	Function:
		Evalates quality values in each of the filtering models

		Returns 1 if the calls[qual_num] needs to be filtered out
		Returns 0 otherwise
*/
int snvmixSkipCall(int *calls, int qual_num, param_struct *params, char *bQ, char *mQ) {
	if(calls[qual_num] == -1)
		return(1);
	if(params->filter_type == TYPE_mb) {
		if(bQ[qual_num] <= params->bQ || mQ[qual_num] <= params->mQ)
			return(1);
	} else if(params->filter_type == TYPE_b) {
		if(bQ[qual_num] <= params->bQ)
			return(1);
	} else {
		if(mQ[qual_num] <= params->mQ)
			return(1);
		if(params->filter_type == TYPE_m) {
			// Use mapping as surrogate
			bQ[qual_num] = mQ[qual_num];
			// Make mapping one
			mQ[qual_num] = (char) PHRED_MAX;
		} else if(params->filter_type == TYPE_M) {
			// Mapping passed, make it one
			mQ[qual_num] = (char) PHRED_MAX;
		} else if(params->filter_type == TYPE_Mb) {
			// Nothing special here
		} else if(params->filter_type == TYPE_MB || params->filter_type == TYPE_SNVMix1) {
			if(bQ[qual_num] <= params->bQ)
				return(1);
			if(params->filter_type == TYPE_SNVMix1) {
				bQ[qual_num] = (char) PHRED_MAX;
				mQ[qual_num] = (char) PHRED_MAX;
		}	}
	}
	return(0);
}



void resetParams(param_struct *params) {
	params->input = stdin;
	params->output = stdout;
	params->inputfile = NULL;
	params->outputfile = NULL;
	params->modelfile = NULL;
	params->cnvfile = NULL;
	params->filter_type = 0;
	params->train = 0;
	params->classify = 0;
	params->filter = 0;
	params->full = 0;
	params->input_type = S_PILEUP; // 0 = processed, 1 = maq pileup, 2 = sam pileup
	params->mu = NULL;
	params->pi = NULL;
	params->max_iter = 1000;
	params->bQ = 19;
	params->mQ = 19;
	params->debug = 0;
	params->max_cn = 2;
	params->min_cn = 2;
	params->trainP.alpha = NULL;
	params->trainP.beta = NULL;
	params->trainP.delta = NULL;
	params->trainP.param_file = NULL;
	params->trainP.max_iter = 100;
	params->trainP.bQ = NULL;
	params->trainP.mQ = NULL;
	params->trainP.calls = NULL;
	params->trainP.cn_state = NULL;
}

void initSNVMix(int argc, char **argv, param_struct *params) {
	char c;
	int i;
	resetParams(params);
	while ((c = getopt (argc, argv, "hDTCFfi:o:m:t:p:q:Q:a:b:d:M:S:c:")) != -1) {
		switch (c)
		{
			case 'm': params->modelfile = optarg; break;
			case 'i': params->inputfile = optarg; break;
			case 'o': params->outputfile = optarg; break;
			case 'T': params->train = 1; break;
			case 'C': params->classify = 1; break;
			case 'F': params->filter = 1; break;
			case 'f': params->full = 1; break;
			case 'p':
				if(*optarg == 'q') {
					params->input_type = Q_PILEUP;
				} else if(*optarg == 'm') {
					params->input_type = M_PILEUP;
				} else if(*optarg == 's') {
					params->input_type = S_PILEUP;
				} else {
					fprintf(stderr,"ERROR: unknown pileup format %c\n",*optarg);
					exit(1);
				}
				break;
			case 't':
				if(strcmp("mb",optarg) == 0)
					params->filter_type = TYPE_mb;
				else if(strcmp("m",optarg) == 0)
					params->filter_type = TYPE_m;
				else if(strcmp("b",optarg) == 0)
					params->filter_type = TYPE_b;
				else if(strcmp("M",optarg) == 0)
					params->filter_type = TYPE_M;
				else if(strcmp("Mb",optarg) == 0)
					params->filter_type = TYPE_Mb;
				else if(strcmp("MB",optarg) == 0)
					params->filter_type = TYPE_MB;
				else if(strcmp("SNVMix1",optarg) == 0)
					params->filter_type = TYPE_SNVMix1;
				else if(strcmp("CoNAn",optarg) == 0)
					params->filter_type = TYPE_CoNAn;
				else {
					fprintf(stderr,"ERROR: filter type '%s' not recognized\n",optarg);
					exit(1);
				}
				break;
			case 'q':
				params->bQ = atoi(optarg);
				if( params->bQ < 0 || params->bQ >= PHRED_MAX )  {
					fprintf(stderr,"ERROR: quality threshold value Q%d out of range\n",params->bQ);
					exit(1);
				}
				break;
			case 'Q':
				params->mQ = atoi(optarg);
				if( params->mQ < 0 || params->mQ >= PHRED_MAX )  {
					fprintf(stderr,"ERROR: mapping threshold value Q%d out of range\n",params->mQ);
					exit(1);
				}
				break;
			case 'h':
				usage(argv[0]);
				break;
			case 'D': params->debug = 1; break;
			case 'M': params->trainP.param_file = optarg; break;
			case 'S':
				params->max_cn = atoi(optarg);
				if( params->max_cn < 2 || params->max_cn > 5 )  {
					fprintf(stderr,"ERROR: max CN state has to be in range [2,5]\n");
					exit(1);
				}
				break;
			case 'c': params->cnvfile = optarg; break;
			default:
				fprintf(stderr,"Unknown option\n");
				usage(argv[0]);
				break;
			}
	}
	/* Decide if we're going to train or classify */
	if(params->filter) {
		params->train = 0;
		params->classify = 0;
	}
	if(params->train && params->classify) {
		fprintf(stderr,"ERROR: choose either train or classify\n");
		exit(1);
	} else if(!params->train && !params->classify && !params->filter) {
		fprintf(stderr,"Train/Classify/Filter not selected, picking default: Classify\n");
		params->classify = 1;
	}

	/* Set parameters */
	allocateParameters(params);
	if( params->train ) setTrainingParameters(params);
	if( params->classify ) setClassificationParameters(params);

	/* Open input and output streams */
	if( params->inputfile != NULL) {
		params->input = fopen(params->inputfile, "r");
		if(params->input == NULL) {
			perror("ERROR: could not open input file");
			exit(1);
		}
	} else {
		params->input = stdin;
	}
	if( params->outputfile != NULL ) {
		params->output = fopen(params->outputfile, "w");
		if(params->output == NULL) {
			perror("ERROR: could not open output file");
			exit(1);
		}
	} else {
		params->output = stdout;
	}

	params->cnv = load_cn_state(params);
	if(!params->cnv && params->filter_type == TYPE_CoNAn) {
		fprintf(stderr,"ERROR: when chosing CoNAn model, CNV file must be provided\n"); exit(1);
	} else if(params->cnv && params->filter_type != TYPE_CoNAn) {
		fprintf(stderr,"ERROR: CNV file only recognized when CoNAn model is selected\n"); exit(1);
	} else if(params->filter_type == TYPE_CoNAn) params->filter_type = TYPE_MB;
}

void allocateParameters(param_struct *params) {

	int cn, cn_states = params->max_cn + 1;
	/* Allocate alpha */
	if( (params->trainP.alpha = malloc(cn_states * sizeof(long double *))) == NULL) {
		perror("ERROR: could not allocate space for alpha\n"); exit(1);
	}
	/* Allocate beta */
	if( (params->trainP.beta = malloc(cn_states * sizeof(long double *))) == NULL) {
		perror("ERROR: could not allocate space for beta\n"); exit(1);
	}
	/* Allocate delta*/
	if( (params->trainP.delta = malloc(cn_states * sizeof(long double *))) == NULL) {
		perror("ERROR: could not allocate space for delta\n"); exit(1);
	}
	/* Allocate mu*/
	if( (params->mu = malloc(cn_states * sizeof(long double *))) == NULL) {
		perror("ERROR: could not allocate space for mu\n"); exit(1);
	}
	/* Allocate pi*/
	if( (params->pi = malloc(cn_states * sizeof(long double *))) == NULL) {
		perror("ERROR: could not allocate space for pi\n"); exit(1);
	}

    for(cn = 0; cn < cn_states; cn++) {
	/* Allocate alpha[cn] */
	if( (params->trainP.alpha[cn] = malloc((cn+1) * sizeof(long double))) == NULL) {
		perror("ERROR: could not allocate space for alpha[cn]\n"); exit(1);
	}
	/* Allocate beta[cn] */
	if( (params->trainP.beta[cn] = malloc((cn+1) * sizeof(long double))) == NULL) {
		perror("ERROR: could not allocate space for beta[cn]\n"); exit(1);
	}
	/* Allocate delta[cn] */
	if( (params->trainP.delta[cn] = malloc((cn+1) * sizeof(long double))) == NULL) {
		perror("ERROR: could not allocate space for delta[cn]\n"); exit(1);
	}
	/* Allocate mu[cn] */
	if( (params->mu[cn] = malloc((cn+1) * sizeof(long double))) == NULL) {
		perror("ERROR: could not allocate space for mu[cn]\n"); exit(1);
	}
	/* Allocate pi[cn] */
	if( (params->pi[cn] = malloc((cn+1) * sizeof(long double))) == NULL) {
		perror("ERROR: could not allocate space for pi[cn]\n"); exit(1);
	}
    }
}

void setTrainingParameters(param_struct *params) {
	if( params->trainP.param_file != NULL ) {
		readParamFile(params,'t');
	} else {
		if(params->max_cn != 2) {
			fprintf(stderr, "ERROR: if state space larger than 3 requested, parameters must be provided\n");
			exit(1);
		}
		fprintf(stderr,"Training parameter file not given, using defaults\n");
		params->trainP.alpha[2][0] = 1000;
		params->trainP.alpha[2][1] = 5000;
		params->trainP.alpha[2][2] = 1;
		params->trainP.beta[2][0] = 1;
		params->trainP.beta[2][1] = 5000;
		params->trainP.beta[2][2] = 1000;
		params->trainP.delta[2][0] = 10;
		params->trainP.delta[2][1] = 1;
		params->trainP.delta[2][2] = 1;
	}
}

void setClassificationParameters(param_struct *params) {
	int k, cn;
	if( params->modelfile != NULL ) {
		readParamFile(params,'c');
	} else {
		if(params->max_cn != 2) {
				fprintf(stderr, "ERROR: if state space larger than 3 requested, model file must be provided\n");
				exit(1);
		}
		params->mu[2][0] = 0.995905287891696078261816182930;
		params->mu[2][1] = 0.499569401000925172873223800707;
		params->mu[2][2] = 0.001002216846753116409260431219;
		params->pi[2][0] = 0.672519580755555956841362785781;
		params->pi[2][1] = 0.139342327124010650907237618412;
		params->pi[2][2] = 0.188138092120433392251399595807;
		fprintf(stderr,"Classification parameter file not given, using for mQ35 bQ10\n");
	}
	fprintf(stderr,"Mu and pi values:\n");
	for(cn = params->min_cn; cn <= params->max_cn; cn++) {
		fprintf(stderr,"cn=%d\n",cn);
		for(k = 0; k <= cn; k++)
			fprintf(stderr,"\tmu[%d][%d] = %Lf\tpi[%d][%d] = %Lf\n", cn, k, params->mu[cn][k], cn, k, params->pi[cn][k]);
		fprintf(stderr,"\n");
	}
}

void readParamFile(param_struct *params, char type) {
	FILE *fptrPARAM;
	char *line = NULL, *param_name, *param, *param_ptr, *filename;
	size_t line_len = 0, read_len = 0;
	long double *target;
	int i, cn;

	if(type == 't') {
		filename=params->trainP.param_file;
	} else if(type == 'c') {
		filename=params->modelfile;
	}
	fptrPARAM=fopen(filename, "r");
	if(fptrPARAM == NULL) {
		fprintf(stderr, "ERROR: could not open parameter file %s\n", filename);
		exit(1);
	}

#if defined __linux__ || defined _GNU_SOURCE
	while( (read_len = getline(&line,&line_len,fptrPARAM)) != -1 )
#endif
#ifdef __APPLE__
	while( (line = fgetln(fptrPARAM,&line_len)) != NULL )
#endif
	{
		line[line_len-1] = '\0';
		target = NULL;
		param_name = strtok_r(line, " ", &param_ptr);
		cn = (int)strtol(strtok_r(param_ptr, " ", &param_ptr), NULL, 10);
		if(cn < params->min_cn || cn > params->max_cn) {
		    fprintf(stderr,"WARN: CN value %d for parameter %s out of range [%d,%d], skipping\n", cn, param_name, params->min_cn,params->max_cn);
		    continue;
		}

		if(type == 't') {
			     if(strcmp("alpha", param_name) == 0) target = params->trainP.alpha[cn];
			else if(strcmp("beta", param_name) == 0)  target = params->trainP.beta[cn];
			else if(strcmp("delta", param_name) == 0) target = params->trainP.delta[cn];
		} else if(type == 'c') {
			     if(strcmp("mu", param_name) == 0) target = params->mu[cn];
			else if(strcmp("pi", param_name) == 0) target = params->pi[cn];

		}
		if(target == NULL) { fprintf(stderr, "Unknown parameter %s, skipping\n", param_name); continue; }

		for(i = 0; i <= cn; i++) {
			param = strtok_r(NULL, " ", &param_ptr);
			if(param == NULL) {
				fprintf(stderr,"ERROR: missing value #%d for %s\n", i, param_name);
				exit(1);
			}
			target[i] = atof(param);
//fprintf(stderr, "DEBUG: %s[%d][%d] = %Lf\n", param_name, cn, i, target[i]);
		}
	}
	fclose(fptrPARAM);
	free(line);
}

void initPhred(long double *phredTable, int elem) {
	int i;
	for(i = 0; i < elem; i++) {
		phredTable[i] = 1-powl(10,(-(long double)i/10));
	}
	phredTable[i] = 1;
}


void usage(char *selfname) {
	param_struct default_params;
	resetParams(&default_params);
	fprintf(stderr,"Syntax:\n\t%s\t-m <modelfile> [-i <infile>] [-o <outfile>] [-T | -C | -F] [-p < q | m | s >] [-t < mb | m | b | M | Mb | MB | SNVMix1>] [-q <#>] [-Q <#>] [-M <trainP file>] [-S <#>] [-c <cvnfile>] [-h]\n",selfname);
	fprintf(stderr,"\tRequired:\n");
	fprintf(stderr,"\t-m <modelfile>\tmodel file, a line for mu and a line for pi, max CNV + 1\n");
	fprintf(stderr,"\t\t\tspace-separated values each, like:\n");
	fprintf(stderr,"\t\t\tmu 0.xxxxxxxx 0.xxxxxxxx 0.xxxxxxxx\n");
	fprintf(stderr,"\t\t\tpi 0.xxxxxxxx 0.xxxxxxxx 0.xxxxxxxx\n");
	fprintf(stderr,"\tOptional:\n");
	fprintf(stderr,"\t-i <infile>\tquality pileup, from pileup2matlab.pl script (def: STDIN)\n");
	fprintf(stderr,"\t-o <outfile>\twhere to put the results (def: STDOUT)\n");
	fprintf(stderr,"\t-T | -C | -F\tTrain, Classify or Filter (def: Classify)\n");
	fprintf(stderr,"\t-p q|m|s\tInput pileup format (def: s)\n\t\t\tq = quality\n\t\t\tm = MAQ\n\t\t\ts = SAMtools\n");
	fprintf(stderr,"\t-t mb|m|b|M|Mb|MB|SNVMix1\n\t\t\tFilter type (def: mb)\n");
	fprintf(stderr,"\t\t\tmb\tLowest between map and base quality\n");
	fprintf(stderr,"\t\t\tm\tFilter on map, and use as surrogate for base quality\n");
	fprintf(stderr,"\t\t\tb\tFilter on base quality, take map quality as 1\n");
	fprintf(stderr,"\t\t\tM\tFilter on map quality but use only base quality\n");
	fprintf(stderr,"\t\t\tMb\tFilter on map quality and use both map and base qualities\n");
	fprintf(stderr,"\t\t\tMB\tFilter on map quality AND base quality\n");
	fprintf(stderr,"\t\t\tSNVMix1\tFilter on base quality and map quality, afterwards consider them perfect\n");
	fprintf(stderr,"\t\t\tCoNAn\tEnable copy number variation data, implies MB (requires -c and -S)\n");
	fprintf(stderr,"\t-q #\t\tCutoff Phred value for Base Quality, anything <= this value is ignored (def: Q%d)\n",default_params.bQ);
	fprintf(stderr,"\t-Q #\t\tCutoff Phred value for Map Quality, anything <= this value is ignored (def: Q%d)\n",default_params.mQ);
	fprintf(stderr,"\n\tCoNAn parameters:\n");
	fprintf(stderr,"\t-c <cnv file>\tFile containing ranges of copy number variation\n");
	fprintf(stderr,"\t-S #\t\tMaximum copy number value (def: %d)\n",default_params.max_cn);
	fprintf(stderr,"\n\tTraining parameters:\n");
	fprintf(stderr,"\t-M <file>\tProvide a file containing training parameters\n");
	fprintf(stderr,"\t\t\tValues are space-separated:\n");
	fprintf(stderr,"\t\t\talpha # # #\n");
	fprintf(stderr,"\t\t\tbeta # # #\n");
	fprintf(stderr,"\t\t\tdelta # # #\n");
	fprintf(stderr,"\n\t-h\t\tthis message\n");
	fprintf(stderr,"\nImplementation: Rodrigo Goya, 2009. Version %s\n",VERSION);
	exit(0);
}

// Based on classify pileup, but reads the entire file to memory for training purposes.
void snvmixGetTrainSet_pileup(param_struct *params) {
//MAQ:
//	1       554484  C       1752    @,,.,.,, @xxx @xxx xxxxx
//SAM:
//	1       554484  C       1752    ,,.,.,,	xxx xxxx

	FILE *fptrIN;

	char *line = NULL;
	size_t line_len = 0, prev_len = 0;
	int read_len = 0;
	int col_num = 0;
	long int line_num = 0;

	char *col, *qual;
	char *col_sep = "\t", *col_str, *col_ptr;
	char *qual_sep = ",", *qual_str, *qual_ptr;
	char *chr, *pos, *ref, nref, call;	

	char *bQ, *mQ;
	int *calls, *tmpQ, pos_int;
	int depth = 0, qual_num,  maxp;
	int depth_allocated = 0;

	int trainset = 0;
	int trainset_allocated = 0;

	int nref_count = 0, ref_count = 0;
	int i, call_i;

	cnv_segment_t cnv_segment;
	cnv_segment.cn = 2;
	cnv_segment.l = 0x7fffffff;
	cnv_segment.u = 0x7fffffff;
	char *cnv_segment_chr = NULL;

	fptrIN = params->input;

	if( (params->trainP.bQ = malloc(START_QNUM * sizeof (unsigned char **))) == NULL) {
		perror("ERROR allocating initial space for train.bQ"); exit(1);
	}
	if( (params->trainP.mQ = malloc(START_QNUM * sizeof (unsigned char **))) == NULL) {
		perror("ERROR allocating initial space for train.mQ"); exit(1);
	}
	if( (params->trainP.calls = malloc(START_QNUM * sizeof (signed char **))) == NULL) {
		perror("ERROR allocating initial space for train.calls"); exit(1);
	}
	if( (params->trainP.depth = malloc(START_QNUM * sizeof (int *))) == NULL) {
		perror("ERROR allocating initial space for train.depth"); exit(1);
	}
	if( (params->trainP.cn_state = malloc(START_QNUM * sizeof (int *))) == NULL) {
		perror("ERROR allocating initial space for train.cn_state"); exit(1);
	}
	trainset_allocated = START_QNUM;

	if( (calls = malloc(START_QNUM * sizeof (int))) == NULL) {
		perror("ERROR allocating initial space for train.depth"); exit(1);
	}
	depth_allocated = START_QNUM;
#if defined __linux__ || defined _GNU_SOURCE
	while( (read_len = getline(&line,&line_len,fptrIN)) != -1 )
#endif
#ifdef __APPLE__
	while( (line = fgetln(fptrIN,&line_len)) != NULL )
#endif
	{
		line[line_len-1] = '\0';
		depth = 0;
		nref = 0;
		col_ptr = NULL;
		for(col_num = 0, col_str = line;  ; col_num++, col_str = NULL) {
			col = strtok_r(col_str, "\t", &col_ptr);
			if(col == NULL) {
				break;
			}
			if(col_num == 0) {
				chr = col;
			} else if(col_num == 1) {
				pos = col;
				pos_int = strtol(pos, NULL, 10);
			} else if(col_num == 2) {
				ref = col;
			} else if(col_num == 3) {
				depth = atoi(col);
				if(depth > depth_allocated) {
					if( (calls = realloc(calls, sizeof (int) * (depth + START_QNUM))) == NULL) {
						perror("ERROR allocating space for calls"); exit(1);
					}
					depth_allocated = depth + START_QNUM;
				}
			} else if(col_num == 4) {
				if(params->input_type == M_PILEUP)
					col = col+sizeof(char);
					snvmixGetCallString(col, calls, depth, &nref);
			} else if(col_num == 5) {
				bQ = col;
				// If it's a MAQ pileup, we need to skip the @ sign
				if(params->input_type == M_PILEUP)
					bQ = bQ + sizeof(char);
				for(call_i = 0; call_i < depth; call_i++)
					bQ[call_i] = bQ[call_i]-33;
			} else if(col_num == 6) {
				mQ = col;
				// If it's a MAQ pileup, we need to skip the @ sign
				if(params->input_type == M_PILEUP)
					mQ = mQ + sizeof(char);
				for(call_i = 0; call_i < depth; call_i++)
					mQ[call_i] = mQ[call_i]-33;
			}
		}
		if(line_num >= trainset_allocated) {
			if( ( params->trainP.bQ = realloc(params->trainP.bQ, (line_num + START_QNUM) * sizeof (unsigned char *)) ) == NULL ) {
				perror("ERROR reallocating space for trainP.bQ"); exit(1);
			}
			if( ( params->trainP.mQ = realloc(params->trainP.mQ, (line_num + START_QNUM) * sizeof (unsigned char *)) ) == NULL ) {
				perror("ERROR reallocating space for trainP.mQ"); exit(1);
			}
			if( ( params->trainP.calls = realloc(params->trainP.calls, (line_num + START_QNUM) * sizeof (signed char *)) ) == NULL) {
				perror("ERROR reallocating space for trainP.calls"); exit(1);
			}
			if( ( params->trainP.depth = realloc(params->trainP.depth, (line_num + START_QNUM) * sizeof (int)) ) == NULL) {
				perror("ERROR reallocating space for trainP.depth"); exit(1);
			}
			if( ( params->trainP.cn_state = realloc(params->trainP.cn_state, (line_num + START_QNUM) * sizeof (int)) ) == NULL) {
				perror("ERROR reallocating space for trainP.cn_state"); exit(1);
			}
			trainset_allocated = line_num + START_QNUM;
		}

		nref = snvmixFilterCalls(calls,depth,bQ,mQ,params);
		nref_count = 0;
		ref_count = 0;
		for(qual_num = 0; qual_num < depth; qual_num++) {
			if(calls[qual_num] == -1)
				continue;
			if(calls[qual_num] == 1)
				ref_count++;
			if(calls[qual_num] == nref)
				nref_count++;
		}
		params->trainP.depth[line_num] = ref_count + nref_count;
		//params->trainP.cn_state[line_num] = get_cn_state(params, chr, strtol(pos,NULL,10));
		// Get CNV at this position
		if(params->cnv) {
			// Need to compare chromosome name
			if(pos_int < cnv_segment.l || pos_int > cnv_segment.u || (cnv_segment_chr == NULL || strcmp(chr,cnv_segment_chr))) {
				get_cn_state(params, chr, pos_int, &cnv_segment);
				free(cnv_segment_chr); cnv_segment_chr = strdup(chr);
			}
			params->trainP.cn_state[line_num] = cnv_segment.cn;
		} else { params->trainP.cn_state[line_num] = 2; }

		if( (params->trainP.bQ[line_num] = malloc(sizeof(unsigned char) * params->trainP.depth[line_num])) == NULL ) {
			perror("ERROR allocating space for trainP.bQ"); exit(1);
		}
		if( (params->trainP.mQ[line_num] = malloc(sizeof(unsigned char) * params->trainP.depth[line_num])) == NULL ) {
			perror("ERROR allocating space for trainP.mQ"); exit(1);
		}
		if( (params->trainP.calls[line_num] = malloc(sizeof(signed char) * params->trainP.depth[line_num])) == NULL ) {
			perror("ERROR allocating space for trainP.calls"); exit(1);
		}

		call_i = 0;
		for(qual_num = 0; qual_num < depth; qual_num++) {
			if(calls[qual_num] == -1)
				continue;
	
			params->trainP.calls[line_num][call_i] = (signed char) calls[qual_num];
			params->trainP.bQ[line_num][call_i] = (unsigned char) bQ[qual_num];
			params->trainP.mQ[line_num][call_i] = (unsigned char) mQ[qual_num];
			call_i++;
		}
		if( params->trainP.depth[line_num] != call_i ) {
			fprintf(stderr, "ERROR: mismatch between trainP.depth and call_i\n"); exit(1);
		}
		line_num++;
	}
	params->trainP.len = line_num;
	params->trainP.len = line_num;
	fclose(fptrIN);
	free(line); free(calls);
}

// Use EM algorithm on a memory-located data set to train the parameters
void snvmixTrain_pileup(param_struct *params) {
	int line_num = 0, call_i = 0, k = 0, cn_states, cn;
	int iter = 0;
	FILE *fptrOUT;

	long double phred[PHRED_MAX + 1];
	long double bsum = 0, msum = 0, **b, **mu, **notmu, **pi, **log_pi, z = 0;
	long double **xrhat, **sum_pG;
	long double **pG, **xr;
	long double Y, not_Y, M;
	int N_sum;
	int strength;

	long double ll, prev_ll = 0;
	//initPhred(phred, PHRED_MAX+1);
	initPhred(phred, PHRED_MAX);

	// Initialize local values of parameters
	mu = params->mu;
	pi = params->pi;
	cn_states = params->max_cn + 1;

	if( ( b = malloc(cn_states * sizeof(long double *)) ) == NULL) { perror("[snvmixClassify_pileup] Could not allocate b\n"); exit(1); }
	if( ( notmu = malloc(cn_states * sizeof(long double *)) ) == NULL) { perror("[snvmixClassify_pileup] Could not allocate notmu\n"); exit(1); }
	if( ( log_pi = malloc(cn_states * sizeof(long double *)) ) == NULL) { perror("[snvmixClassify_pileup] Could not allocate log_pi\n"); exit(1); }
	if( ( xrhat = malloc(cn_states * sizeof(long double *)) ) == NULL) { perror("[snvmixClassify_pileup] Could not allocate xrhat\n"); exit(1); }
	if( ( sum_pG = malloc(cn_states * sizeof(long double *)) ) == NULL) { perror("[snvmixClassify_pileup] Could not allocate sum_pG\n"); exit(1); }

	for(cn = params->min_cn; cn <= params->max_cn; cn++) {
		if( ( b[cn] = malloc((cn+1) * sizeof(long double)) ) == NULL) { perror("[snvmixClassify_pileup] Could not allocate b[cn]\n"); exit(1); }
		if( ( notmu[cn] = malloc((cn+1) * sizeof(long double)) ) == NULL) { perror("[snvmixClassify_pileup] Could not allocate notmu[cn]\n"); exit(1); }
		if( ( log_pi[cn] = malloc((cn+1) * sizeof(long double)) ) == NULL) { perror("[snvmixClassify_pileup] Could not allocate log_pi[cn]\n"); exit(1); }
		if( ( xrhat[cn] = malloc((cn+1) * sizeof(long double)) ) == NULL) { perror("[snvmixClassify_pileup] Could not allocate xrhat[cn]\n"); exit(1); }
		if( ( sum_pG[cn] = malloc((cn+1) * sizeof(long double)) ) == NULL) { perror("[snvmixClassify_pileup] Could not allocate sum_pG[cn]\n"); exit(1); }
	}

	snvmixGetTrainSet_pileup(params);

	if(params->debug) {
	for(line_num = 0; line_num < params->trainP.len; line_num++) {
		fprintf(stderr, "line_num: %d", line_num);
		for(call_i = 0; call_i < params->trainP.depth[line_num]; call_i++) {
			fprintf(stderr, "\t(%d,bQ%d,mQ%d)", params->trainP.calls[line_num][call_i], params->trainP.bQ[line_num][call_i], params->trainP.mQ[line_num][call_i]);
		}
		fprintf(stderr, "\n\n");
	}
	}
	// Initialize mu and pi

	fprintf(stderr, "Initializing mu\n");
	for(cn = params->min_cn; cn <= params->max_cn; cn++) {
	    fprintf(stderr,"cn = %d\n", cn);
	    for(k = 0; k <= cn; k++) {
			params->mu[cn][k] = (long double) params->trainP.alpha[cn][k] / (params->trainP.alpha[cn][k] + params->trainP.beta[cn][k]);
			fprintf(stderr,"\talpha[%d][%d] = %Lf,\tbeta[%d][%d] = %Lf,\tmu[%d][%d] = %Lf\n", cn, k, params->trainP.alpha[cn][k], cn, k, params->trainP.beta[cn][k], cn, k, params->mu[cn][k]);
		}
	}

	fprintf(stderr, "Initializing pi\n");
	for(cn = params->min_cn; cn <= params->max_cn; cn++) {
		fprintf(stderr,"cn = %d\n", cn);
		z = 0;
		for(k = 0; k <= cn; k++)
			z += (long double) params->trainP.delta[cn][k];
		if(!z) { z = 1; }
		for(k = 0; k <= cn; k++) {
			params->pi[cn][k] = (long double) params->trainP.delta[cn][k]  / z;
			fprintf(stderr,"\tdelta[%d][%d] = %Lf,\tpi[%d][%d] = %Lf\n", cn, k, params->trainP.delta[cn][k], cn, k, params->pi[cn][k]);
		}
	}

	strength = params->trainP.len;

	// Initialize matrices
	if( (pG = malloc(sizeof(long double *) * params->trainP.len)) == NULL) {
		perror("ERROR allocating initial space for pG"); exit(1);
	}
	if( (xr = malloc(sizeof(long double *) * params->trainP.len)) == NULL) {
		perror("ERROR allocating initial space for xr"); exit(1);
	}
	for(line_num = 0; line_num < params->trainP.len; line_num++) {
		// [genotypes] cells for each line_num
		if( (pG[line_num] = malloc(sizeof(long double) * params->trainP.cn_state[line_num])) == NULL) {
			perror("ERROR allocating space for pG"); exit(1);
		}
		if( (xr[line_num] = malloc(sizeof(long double) * params->trainP.cn_state[line_num])) == NULL) {
			perror("ERROR allocating space for xr"); exit(1);
		}
	}

	for(iter = 0; iter < params->trainP.max_iter; iter++) {
		// Reset values for this iteration
	    for(cn = params->min_cn; cn <= params->max_cn; cn++) {
		for(k = 0; k <= cn; k++) {
			notmu[cn][k] = 1 - mu[cn][k];
			log_pi[cn][k] = logl(pi[cn][k]);

			sum_pG[cn][k] = 0;
			xrhat[cn][k] = 0;

		}
	    }
		N_sum = 0;
		ll = 0;

		// E step
		for(line_num = 0; line_num < params->trainP.len; line_num++) {
			cn = params->trainP.cn_state[line_num];
			if(params->trainP.depth == 0)
				continue;
			for(k = 0; k <= cn; k++) {
				xr[line_num][k] = 0;
				b[cn][k] = 0;
			}

			for(call_i = 0; call_i < params->trainP.depth[line_num]; call_i++) {
				if(params->trainP.calls[line_num][call_i] == 1) {
					Y = phred[params->trainP.bQ[line_num][call_i]];
					not_Y = 1-phred[params->trainP.bQ[line_num][call_i]];
				} else {
					Y = (1-phred[params->trainP.bQ[line_num][call_i]])/3;
					not_Y = phred[params->trainP.bQ[line_num][call_i]] + 2*(1-phred[params->trainP.bQ[line_num][call_i]])/3;
				}
				M =  phred[params->trainP.mQ[line_num][call_i]];
				for(k = 0; k <= cn; k++) {
					b[cn][k] = b[cn][k] + logl( (1 - M) * 0.5 + M * (Y * mu[cn][k] + not_Y * notmu[cn][k]) );
					xr[line_num][k] = xr[line_num][k] + Y;
				}
			}
			z = 0;
			for(k = 0; k <= cn; k++) {
				b[cn][k] = expl(b[cn][k] + log_pi[cn][k]);
				z = z + b[cn][k];
			}
			if(!z) { z=1; }
			for(k = 0; k <= cn; k++) {
				pG[line_num][k] = b[cn][k] / z;
			}

			ll = ll + logl(z);

			N_sum = N_sum + params->trainP.depth[line_num];

			for(k = 0; k <= cn; k++) {
				sum_pG[cn][k] = sum_pG[cn][k] + pG[line_num][k];
				xrhat[cn][k] = xrhat[cn][k] + xr[line_num][k] * pG[line_num][k];
			}
		}

		// Check log likelihood
		if(iter == 0)
			prev_ll = ll;
		else if(ll <= prev_ll)
			break;
		prev_ll = ll;

		// M step
		z = 0;
		for(k = 0; k <= cn; k++) {
			z = z + sum_pG[cn][k] + params->trainP.delta[cn][k];
		}
		if(!z) { z=1; }
		for(k = 0; k <= cn; k++) {
			// Update pi
			params->pi[cn][k] = (sum_pG[cn][k] + params->trainP.delta[cn][k]) / z;
			// Update mu
			params->mu[cn][k] = (xrhat[cn][k] + params->trainP.alpha[cn][k]*strength-1) / (N_sum + params->trainP.alpha[cn][k]*strength + params->trainP.beta[cn][k]*strength-2);
		}
	}

	if(params->modelfile == NULL) {
		fptrOUT = stdout;
	} else {
		fptrOUT = fopen(params->modelfile, "w");
		if(fptrOUT == NULL) {
			perror("ERROR: could not open modelfile for writing, using STDOUT");
			fptrOUT = stdout;
		}
	}
	for(cn = params->min_cn; cn <= params->max_cn; cn++) {
		fprintf(fptrOUT,"mu %d", cn);
		for(k = 0; k <= cn; k++) {
			fprintf(fptrOUT, " %0.30Lf", params->mu[cn][k]);
		}
		fprintf(fptrOUT,"\n");
	}
	for(cn = params->min_cn; cn <= params->max_cn; cn++) {
		fprintf(fptrOUT,"pi %d", cn);
		for(k = 0; k <= cn; k++) {
			fprintf(fptrOUT, " %0.30Lf", params->pi[cn][k]);
		}
		fprintf(fptrOUT,"\n");
	}
	/* free unused memory */
	for(cn = params->min_cn; cn <= params->max_cn; cn++) {
		free(b[cn]); free(notmu[cn]); free(log_pi[cn]);
		free(xrhat[cn]); free(sum_pG[cn]);
	}
	free(b); free(notmu); free(log_pi);
	free(xrhat); free(sum_pG);
	for(line_num = 0; line_num < params->trainP.len; line_num++) {
		// free [genotypes] cells for each line_num
		free(pG[line_num]);
		free(xr[line_num]);
	}
	free(pG); free(xr);
}


khash_t(h_cnv) *load_cn_state(param_struct *params) {
	cnv_segment_t *cnv_segment;
	cnv_seq_t *cnv_seq;
	khiter_t k;
	FILE *fptr;
	int ret, i, l, u, cn;

	char *seqid, *col, *col_str, *line, *col_ptr;
	int linenum, col_num;
	size_t line_len, read_len;

	khash_t(h_cnv) *cnv = kh_init(h_cnv);

	if( params->cnvfile != NULL) {
		fptr = fopen(params->cnvfile, "r");
		if(fptr == NULL) {
			perror("ERROR: could not open cnvfile file");
			exit(1);
		}
	} else {
		params->cnv = NULL;
		return(0);
	}
	linenum = 0;
	line_len = 0;
	line = NULL;
#if defined __linux__ || defined _GNU_SOURCE
	while( (read_len = getline(&line,&line_len,fptr)) != -1 )
#endif
#ifdef __APPLE__
	while( (line = fgetln(fptr,&line_len)) != NULL )
#endif
	{
		line[line_len-1] = '\0';
		linenum++;
		col_ptr = NULL;
		for(col_num = 0, col_str = line; ; col_num++, col_str = NULL) {
			col = strtok_r(col_str, "\t", &col_ptr);
			if(col == NULL) {
				break;
			}
			if(col_num == 0) {
				seqid = col;
			} else if(col_num == 1) {
			    	l = strtol(col, NULL, 10);
			} else if(col_num == 2) {
			    	u = strtol(col, NULL, 10);
			} else if(col_num == 3) {
			    	cn = strtol(col, NULL, 10);
			}
		}
		if(col_num != 4) {
			fprintf(stderr, "WARNING: cannot read CNV interval in line %d of CNV file, skipping\n", linenum);
			continue;
		}

		// Check if seqid exists, else create
		k = kh_get(h_cnv, cnv, seqid);
		if(k == kh_end(cnv)) {
			cnv_seq = malloc(sizeof(cnv_seq_t));
			cnv_seq->seqid = strdup(seqid);
			cnv_seq->n_segments = 0;
			cnv_seq->segments = NULL;

			k = kh_put(h_cnv, cnv, cnv_seq->seqid, &ret);
			if(!ret) {
				kh_del(h_cnv, cnv, k);
				fprintf(stderr, "WARNING: error inserting hash element for line %d of CNV file, skipping\n", linenum);
				continue;
			}
			kh_value(cnv, k) = cnv_seq;
		} else {
			cnv_seq = kh_value(cnv, k);
		}

		// Create segment record and add
		cnv_seq->n_segments++;
		cnv_seq->segments = realloc(cnv_seq->segments, (cnv_seq->n_segments) * sizeof(cnv_segment_t));
		if(cnv_seq->segments == NULL) { perror("ERROR: error found reallocating space for CNV interval reading\n"); exit(1); }
		cnv_segment = cnv_seq->segments + cnv_seq->n_segments - 1;
		cnv_segment->l = l;
		cnv_segment->u = u;
		cnv_segment->cn = cn;
	}

	// Sort each segment array in the hash by l position
	for(k = kh_begin(cnv); k != kh_end(cnv); k++) {
		if(kh_exist(cnv, k)) cnv_seq = kh_value(cnv, k);
		else continue;

		uint64_t *sort_helper;
		cnv_segment_t *tmp_segments;
		int j;
		sort_helper = malloc(cnv_seq->n_segments * sizeof(uint64_t));
		tmp_segments = malloc(cnv_seq->n_segments * sizeof(cnv_segment_t));

		for(i = 0; i < cnv_seq->n_segments; i++)
			sort_helper[i] = ((uint64_t )cnv_seq->segments[i].l) << 32 | i;
		ks_introsort(uint64_t, cnv_seq->n_segments, sort_helper);
		for(i = 0; i < cnv_seq->n_segments; i++) {
			j = sort_helper[i] & 0xffffffff;
			tmp_segments[i].l = cnv_seq->segments[j].l;
			tmp_segments[i].u = cnv_seq->segments[j].u;
			tmp_segments[i].cn = cnv_seq->segments[j].cn;
		}

		free(sort_helper); free(cnv_seq->segments);
		cnv_seq->segments = tmp_segments;
	}
	return(cnv);
}

void get_cn_state(param_struct *params, char *seqid, int pos, cnv_segment_t *cnv_segment) {
	khiter_t k;
	khash_t(h_cnv) *cnv = params->cnv;
	cnv_seq_t *cnv_seq;
	uint32_t l = 0;				// min pos
	uint32_t u = 0x7fffffff;	// max pos
	char *s_seqid = NULL;
	int i, cn = 2;					// default cn

	//fprintf(stderr, "DEBUG: in get_cn_state\n");
	k = kh_get(h_cnv, cnv, seqid);
	if(k != kh_end(cnv)) {
		cnv_seq = kh_value(cnv, k);
		cnv_segment_t *s = cnv_seq->segments;
		for(i = 0; i < cnv_seq->n_segments; i++) {
			if(pos < s[i].l) {
				l++;
				u = s[i].l - 1;
				break;
			} else if(pos >= s[i].l && pos <= s[i].u) {
				l = s[i].l;
				u = s[i].u;
				cn = s[i].cn;
				break;
			} else if(pos > s[i].u && i == (cnv_seq->n_segments - 1)) {
				l = s[i].u + 1;
				u = 0x7fffffff;
				break;
			}
		}
	}
	// else { Sequence is not in CNV definition, thus we leave the defaults }
	if(cn > params->max_cn || cn < params->min_cn) {
		fprintf(stderr, "WARNING: copy number value for %s:%d-%d is out of range, making 2\n", seqid, l, u, params->min_cn, params->max_cn);
		cn = 2;
	}
	cnv_segment->cn = cn;
	cnv_segment->l = l;
	cnv_segment->u = u;
}
